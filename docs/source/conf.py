"""Configuration file for the Sphinx documentation builder for elise_client."""
# -- Path setup --------------------------------------------------------------
import sys
from pathlib import Path
from typing import List

import sphinx_rtd_theme

PROJECT_ROOT = Path(__file__).parents[2] / "src"
print(f"DOCS: PROJECT_ROOT = {PROJECT_ROOT.absolute()}", file=sys.stderr)
sys.path.insert(0, str(PROJECT_ROOT))
print(f"DOCS: sys.path = {sys.path}", file=sys.stderr)

# -- Project information -----------------------------------------------------

project = "elise-client"
copyright = "2021, François GUÉRIN <fguerin@ville-tourcoing.fr>"  # noqa
author = "François GUÉRIN <fguerin@ville-tourcoing.fr>"

# The full version, including alpha/beta/rc tags
version = "0.5"
release = "0.5.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinxcontrib.plantuml",
]

plantuml = "plantuml.sh"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns: List[str] = []


# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# html_static_path = ["_themes/sphinx_rtd_theme/static"]

html_sidebars = {
    "**": [
        "relations.html",  # needs 'show_related': True theme option to display
        "searchbox.html",
    ]
}

# Intersphinx
intersphinx_mapping = {
    "python": ("https://docs.python.org/", None),
    "requests": ("https://docs.python-requests.org/en/master/", None),
}

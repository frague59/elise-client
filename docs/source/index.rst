.. elise-client documentation master file, created by
   sphinx-quickstart on Mon Jun 14 15:30:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to elise-client's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   elise_client


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

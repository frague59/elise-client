# Elise Client

[Elise](https://www.neoledge.com/fr/produits/logiciel-ged-ecm-elise/) is GED product.

A client for Elise API.

## Docs

+ http://fguerin.docs.tourcoing.fr/elise-client/

## Project

+ https://gitlab.ville.tg/fguerin/elise-client


## Provided API endpoints

+ Create a token
+ Send files to the server
+ Create a `Procedure` with those files
+ View `Procedure` data
+ Delete (cancel) a `Procedure`

## Usage: Use the `with` statement !

```python
import elise_client
with elise_client.EliseClient(
        base_url=<base_url>,
        instance_name=<instance_name>,
        application_key=<application_key>,
        username=<username>,
        password=<password>,
) as client:
    file_path = Path(<Path to the file too upload>)
    file_struct = client.send_file(file_path=file_path)
    procedure_id = client.create_procedure(
        rack_code=<rack_code>,
        name=<procedure_name>,
        input_files=[file_struct],
    )
    procedure = client.get_procedure(proc_id=procedure_id)
```

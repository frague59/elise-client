"""
Test helpers against ELISE instance.

:creationdate: 08/06/2021 16:15
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: tests.test_helpers
"""
from typing import Any, Dict, List

import pytest  # noqa

from elise_client import helpers

# region Fixtures

URL_DATA_LIST: List[Dict[str, Any]] = [
    {
        "attended": "https://tourcoingv6.illico.city:1443/api/Tokens?api-version=1.0",
        "base_url": "https://tourcoingv6.illico.city:1443",
        "url": "api/Tokens",
        "query_params": {"api-version": "1.0"},
    },
    {
        "attended": "https://tourcoingv6.illico.city:1443/api/Files?api-version=1.0",
        "base_url": "https://tourcoingv6.illico.city:1443",
        "url": "api/Files",
        "query_params": {"api-version": "1.0"},
    },
]


@pytest.fixture(scope="module", params=URL_DATA_LIST)
def url_data(request) -> Dict[str, Any]:
    """
    Get params to test the format_url method.

    .. warning: Test fixture !

    :return: URL data
    """

    return request.param


# endregion Fixtures


def test_format_url(url_data: Dict[str, Any]) -> None:
    """
    Test the :meth:`elise_client.client.EliseClient.format_url()` method against url_data.

    :param url_data: Data dict
    :return: Nothing
    """
    attended = url_data.pop("attended")
    assert helpers.format_url(**url_data) == attended

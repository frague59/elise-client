"""
Test models helpers against ELISE instance.

:creationdate: 08/06/2021 16:15
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: tests.test_helpers
"""
from typing import Any, Dict, List

import pytest  # noqa

from elise_client.models import helpers

CHANGE_CASE_LIST: List[Dict[str, str]] = [
    {
        "camel": "sequentialIdentifier",
        "camel_upper": "SequentialIdentifier",
        "snake": "sequential_identifier",
    },
    {
        "camel": "externalRecipients",
        "camel_upper": "ExternalRecipients",
        "snake": "external_recipients",
    },
    {
        "camel": "documentData",
        "camel_upper": "DocumentData",
        "snake": "document_data",
    },
    {
        "camel": "mimeType",
        "camel_upper": "MimeType",
        "snake": "mime_type",
    },
    {
        "camel": "isDigitallySigned",
        "camel_upper": "IsDigitallySigned",
        "snake": "is_digitally_signed",
    },
    {
        "camel": "eventActor",
        "camel_upper": "EventActor",
        "snake": "event_actor",
    },
    {
        "camel": "workflowHistory",
        "camel_upper": "WorkflowHistory",
        "snake": "workflow_history",
    },
]


@pytest.fixture(scope="module", params=CHANGE_CASE_LIST)
def change_case(request) -> Dict[str, str]:
    """
    Get params to test the to_camel_case function.

    .. warning: Test fixture !

    :return: JSON data
    """
    return request.param


def test_to_camel_case(change_case):
    """
    Test the :meth:`elise_client.models.helpers.to_camel_case()` method.

    :param change_case: Dict {"camel": <camelSaseString>, "snake": "snake_case_string"}
    :return: Nothing
    """
    assert helpers.to_camel_case(change_case["snake"]) == change_case["camel"]


def test_to_camel_case_upper(change_case):
    """
    Test the :meth:`elise_client.models.helpers.to_camel_case()` method with 1rst letter upper cased..

    :param change_case: Dict {"camel": <camelSaseString>, "snake": "snake_case_string"}
    :return: Nothing
    """
    assert helpers.to_camel_case(change_case["snake"], True) == change_case["camel_upper"]


def test_to_snake_case(change_case):
    """
    Test the :meth:`elise_client.models.helpers.to_snake_case()` method.

    :param change_case: Dict {"camel": <camelSaseString>, "snake": "snake_case_string"}
    :return: Nothing
    """
    assert helpers.to_snake_case(change_case["camel"]) == change_case["snake"]

"""
Test authentication against ELISE instance.

:creationdate: 25/05/2021 16:15
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: tests.test_authentication
"""
import re
import sys

import pytest  # noqa

from elise_client import EliseClient, helpers

from .fixtures import *  # noqa


@pytest.mark.dependency()
def test_authentication(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
) -> None:
    """
    Test the authentication routines.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        assert client
        assert client.is_connected


@pytest.mark.dependency(depends=["test_authentication"])
def test_create_procedure(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
    rack_code: str,
    pdf_file_path: Path,  # noqa
) -> None:
    """
    Test the procedure creation.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :param rack_code: Rack code
    :param pdf_file_path: File to upload to the server
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        # Upload the file
        file_input = client.send_file(
            file_path=pdf_file_path,
        )
        assert file_input
        assert isinstance(file_input, models.FileInput)
        assert re.compile(r"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}").match(file_input.id) is not None

        # Create the procedure
        name = f"Test procedure for {pdf_file_path.name} on {rack_code}"
        api_response = client.create_procedure(
            rack_code=rack_code,
            name=name,
            input_files=[file_input],
        )
        assert api_response
        assert api_response.data
        assert isinstance(api_response.data, str)
        assert re.compile(r"\w+_\d+", re.UNICODE).match(api_response.data) is not None
        pytest.procedure = {"id": api_response.data}  # type: ignore


@pytest.mark.dependency(depends=["test_create_procedure[pdf_file_path0]"])
def test_get_procedure(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
) -> None:
    """
    Test the procedure getting.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        api_response = client.get_procedure(proc_id=pytest.procedure.get("id"))  # type: ignore # noqa
        assert api_response
        assert api_response.data
        assert api_response.data.id == pytest.procedure.get("id")  # type: ignore # noqa
        assert api_response.data.files  # type: ignore
        pytest.procedure = api_response.data  # type: ignore


@pytest.mark.dependency(depends=["test_get_procedure"])
def test_get_procedure_file(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
    file_format: str,
) -> None:
    """
    Test the procedure getting.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :param file_format: Output file format
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        for file_data in pytest.procedure.files:  # type: ignore # noqa
            try:
                procedure_file = client.get_procedure_file(
                    proc_id=pytest.procedure.id,  # type: ignore # noqa
                    file_id=file_data.id,
                    file_format=models.ProcedureFileFormat(file_format),
                )
            except helpers.ClientError as e:
                print(e, file=sys.stderr)
            else:
                assert procedure_file
                assert isinstance(procedure_file, bytes)


@pytest.mark.dependency(depends=["test_get_procedure"])
def test_delete_procedure(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
) -> None:
    """
    Test the procedure getting.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        api_response = client.delete_procedure(proc_id=pytest.procedure.id)
        assert api_response.status_code == 204


@pytest.mark.dependency(depends=[""])
def test_create_final_procedure(
    base_url: str,
    instance_name: str,
    application_key: str,
    username: str,
    password: str,
    final_rack_code: str,
    final_external_recipients: List[str],
    pdf_file_path: Path,  # noqa
) -> None:
    """
    Test create a final procedure, with external recipients.

    :param base_url: Elise Server USL
    :param instance_name: Elise instance name
    :param application_key: Elise application key
    :param username: Connection username
    :param password: Connection password
    :param final_rack_code: Final procedure rack name
    :param final_external_recipients: List of external recipients
    :param pdf_file_path: PDF File path
    :return: Nothing
    """
    with EliseClient(
        base_url=base_url,
        instance_name=instance_name,
        application_key=application_key,
        username=username,
        password=password,
    ) as client:
        # Upload the file
        file_input = client.send_file(
            file_path=pdf_file_path,
        )
        assert file_input
        assert isinstance(file_input, models.FileInput)
        assert re.compile(r"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}").match(file_input.id) is not None

        # Create the procedure
        name = f"Test final procedure for {pdf_file_path.name} on {rack_code}"
        proc_id = client.create_procedure(
            rack_code=final_rack_code,
            name=name,
            input_files=[file_input],
            external_recipients=final_external_recipients,
        )
        assert proc_id
        assert isinstance(proc_id, str)
        assert re.compile(r"\w+_\d+", re.UNICODE).match(proc_id) is not None
        pytest.procedure = {"id": proc_id}  # type: ignore

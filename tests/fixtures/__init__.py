"""
common for :mod:`elise_client` application tests.

:creationdate: 25/05/2021 16:15
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: tests.common
"""
from pathlib import Path
from typing import Generator, List

import dotenv  # noqa
import pytest  # noqa

from elise_client import models

__author__ = "fguerin"
CONFIG = dotenv.dotenv_values()


@pytest.fixture(scope="session")
def base_url() -> str:
    """
    Get the API hostname from env file.

    .. warning: Test fixture !

    :return: Hostname
    """
    return CONFIG.get("base_url")


@pytest.fixture(scope="session")
def username() -> str:
    """
    Get the API username from env file.

    .. warning: Test fixture !

    :return: Username
    """
    return CONFIG.get("username")


@pytest.fixture(scope="session")
def password() -> str:
    """
    Get the API password from env file.

    .. warning: Test fixture !

    :return: Username
    """
    return CONFIG.get("password")


@pytest.fixture(scope="session")
def application_key() -> str:
    """
    Get the API application_key from env file.

    .. warning: Test fixture !

    :return: Username
    """
    return CONFIG.get("application_key")


@pytest.fixture(scope="session")
def instance_name() -> str:
    """
    Get the API instance_name from env file.

    .. warning: Test fixture !

    :return: Username
    """
    return CONFIG.get("instance_name")


def get_pdf_file_path() -> Generator[Path, None, None]:
    """
    Get the files in the ./test_files directory and yields the results
    :return:
    """
    test_dir = Path(__file__).parent / "test_files"
    for test_file in test_dir.iterdir():
        if not test_file.is_file():
            continue
        yield test_file


@pytest.fixture(scope="session", params=list(get_pdf_file_path()))
def pdf_file_path(request) -> Path:
    """
    Get the files in the ./test_files directory and yields the results

    .. warning: Test fixture !

    :return: Path for a file
    """
    return request.param


@pytest.fixture(scope="session")
def rack_code() -> str:
    """
    Get the API instance_name from env file.

    .. warning: Test fixture !

    :return: Username
    """
    return CONFIG.get("rack_code")


@pytest.fixture(scope="module", params=[_format.value for _format in models.ProcedureFileFormat])
def file_format(request) -> str:
    """
    Yield the procedure file formats.

    .. warning: Test fixture !

    :return: Procedure format as str
    """
    return request.param


# region Final validation
@pytest.fixture(scope="session")
def final_rack_code() -> str:
    """
    Get the API `final_rack_code` from env file.

    .. warning: Test fixture !

    :return: Final rack code
    """
    return CONFIG.get("final_rack_code")


@pytest.fixture(scope="session")
def final_external_recipients() -> List[str]:
    """
    Get the API `final_external_recipients` from env file.

    .. warning: Test fixture !

    :return: List of email addresses
    """
    final_external_recipients = CONFIG.get("final_external_recipients")
    return final_external_recipients.split(",")


# endregion Final validation

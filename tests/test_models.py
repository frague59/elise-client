"""
Test the :mod:`elise_client.models` module.

:creationdate: 07/06/2021 16:20
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise-client.test_models

"""
from typing import Any, Dict, List

import pytest  # noqa

from elise_client import models

__author__ = "fguerin"

# region Fixtures

JSON_DATA_LIST: List[Dict[str, Any]] = [
    {
        "instance": models.Metadata(name="foo", value="bar"),
        "attended": {"name": "foo", "value": "bar"},
    },
    {
        "instance": models.DocumentData(
            sequential_identifier="xyz",
            name="foo",
            description="bar",
            comment="baz",
            reference="qux",
            confidentiality=models.ConfidentialityLevel.Normal,
            priority=models.PriorityLevel.Normal,
            external_recipients=[
                "foo@example.com",
                "bar@example.com",
            ],
            metadatas=[
                models.Metadata(name="foo", value="bar"),
                models.Metadata(name="baz", value="qux"),
            ],
        ),
        "attended": {
            "sequentialIdentifier": "xyz",
            "name": "foo",
            "description": "bar",
            "comment": "baz",
            "reference": "qux",
            "confidentiality": "Normal",
            "priority": "Normal",
            "externalRecipients": [
                "foo@example.com",
                "bar@example.com",
            ],
            "metadatas": [
                {"name": "foo", "value": "bar"},
                {"name": "baz", "value": "qux"},
            ],
        },
    },
]


@pytest.fixture(scope="module", params=JSON_DATA_LIST)
def json_data(request) -> Dict[str, Any]:
    """
    Get params to test the to_json_data method.

    .. warning: Test fixture !

    :return: JSON data
    """

    return request.param


# endregion Fixtures


def test_model_to_json_data(json_data: Dict[str, Any]) -> None:
    """
    Test the :class:`elise_client.models.Model` serialization as JSON-serialized dict.

    :param json_data: Data dict and output_model instance
    :return: Nothing
    """
    assert isinstance(json_data["instance"], models.Model)
    _data = json_data["instance"].to_json_data()
    assert isinstance(_data, dict)
    assert _data == json_data["attended"]


def test_json_data_to_model(json_data: Dict[str, Any]) -> None:
    """
    Test the JSON-serialized dict DE-serialization as :class:`elise_client.models.Model`.

    :param json_data: Data dict and output_model instance
    :return: Nothing
    """
    model_class = json_data["instance"].__class__
    from_json = model_class.from_json_data(json_data["attended"])
    assert from_json == json_data["instance"]

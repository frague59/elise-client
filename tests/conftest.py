"""
conftest form :mod:`tests` application.

:creationdate: 11/06/2021 11:35
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: tests.conftest

"""
import pytest  # noqa


def file_plugin():
    """Simple plugin to save file data between tests."""
    return {
        "file_id": "file_id",
        "file_name": "file_name",
    }


def procedure_plugin():
    """Simple plugin to save procedure data between tests."""
    return {
        "procedure_id": "procedure_id",
    }


def pytest_configure():
    """Add a `file_data` and `procedure` parameters to pytest namespace."""
    pytest.file = file_plugin()
    pytest.procedure = procedure_plugin()

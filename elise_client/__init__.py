"""
Client for connections with `Elise application <https://www.neoledge.com/fr/produits/logiciel-ged-ecm-elise/>`_.

:creationdate: 25/05/2021 16:10
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client

"""
__version__ = VERSION = "0.5.0"

from .client import EliseClient

__all__ = ("EliseClient",)

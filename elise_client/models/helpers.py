"""
helpers for :mod:`elise_client.models` application.

:creationdate: 07/06/2021 15:10
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client.models.helpers

"""
import logging
from typing import Optional

import inflection

__author__ = "fguerin"
logger = logging.getLogger("elise_client.models.helpers")


class ModelError(Exception):
    """An error on output_model manipulation."""

    pass


class ModelValidationError(ModelError):
    """Cannot validate the output_model."""

    def __init__(self, field_name: str, message: Optional[str] = None):
        """
        Initialize the ModelValidationError.

        :param field_name: Field name
        :param message: Extra message
        """
        super().__init__()
        self._message = message
        self._field_name = field_name

    def __str__(self):
        """Display the error message."""
        return f"{self._field_name}: {self._message}"


def to_camel_case(name: str, uppercase_first_letter: bool = False) -> str:
    """
    Convert a string to camelCase.

    :param name: string to convert
    :param uppercase_first_letter: If `True`, the first lettre will be upper-ized
    :return: camelCaseString
    """
    return inflection.camelize(name, uppercase_first_letter)


def to_snake_case(name: str) -> str:
    """
    Convert a string to snake_case.

    :param name: string to convert
    :return: snake_case_string
    """
    return inflection.underscore(name)

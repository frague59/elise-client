"""
Models for :mod:`elise_client` application.

:creationdate: 25/05/2021 16:34
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client.models

"""
from __future__ import annotations

import enum
import logging
from dataclasses import dataclass, fields
from datetime import datetime
from typing import Any, Dict, List, Optional, Union, get_args, get_origin, get_type_hints

from .enums import ProcedureFileFormat  # noqa
from .enums import ConfidentialityLevel, PriorityLevel, ProcedureStatus, WorkflowTaskStatus, WorkflowTaskType
from .helpers import ModelValidationError, to_camel_case, to_snake_case

logger = logging.getLogger("elise_client.models")
__author__ = "fguerin"


@dataclass()
class Model:
    """
    Abstract output_model.

    .. note:: This classes implements the __init__ method
    """

    def __init__(self, **kwargs):  # noqa: CCR001
        """
        Initialize the output_model instance.

        .. note:: This method is used for __ALL__ sub-classes...

        :param kwargs: JSON-parsed raw json_data.
        """

        def is_not_magic(x: str) -> bool:
            return not x.startswith("__")

        for key, val in kwargs.items():
            if key not in dir(self):
                raise ModelValidationError(
                    key,
                    f"Not in {self.__class__.__name__} output_model - "
                    f"Might be in {list(filter(is_not_magic, dir(self)))}.",
                )
            _val = val
            setattr(self, key, _val or val)
        if hasattr(self, "clean") and callable(self.clean):
            self.clean()

    @staticmethod
    def get_json_value(val) -> Any:
        """
        Get the JSON-serializable value.

        :param val: value
        :return: JSON-serializable value
        """
        if isinstance(val, Model):
            return val.to_json_data()
        if isinstance(val, enum.Enum):
            return val.value
        if isinstance(val, list):
            val_json_data = []
            for _v in val:
                val_json_data.append(Model.get_json_value(_v))
            return val_json_data
        return val

    @classmethod
    def get_cleaned_value(cls, field_name: str, value: Any) -> Any:
        """
        Get a cleaned value for de-serialization.

        :param field_name: Field name
        :param value: Field value
        :return: De-serialized value
        """
        field_type = get_type_hints(cls)[field_name]
        logger.debug(f"Model::get_cleaned_value({field_name}, {value}) field_name = {field_name}")
        return cls.get_value_for_type(field_type=field_type, value=value)

    def to_json_data(self) -> Dict[str, Any]:
        """
        Convert Model instance into a serializable JSON data dict.

        :return: Serializable JSON data dict
        """
        json_data = {}
        for field in fields(self):
            val = getattr(self, field.name)
            computed_val = self.get_json_value(val)
            logger.debug(f"Model::to_json_data() val: {val} / computed_val: {computed_val}")
            json_data.update({to_camel_case(field.name): computed_val})
        return json_data

    @classmethod
    def get_value_for_type(cls, field_type: Any, value: Any) -> Any:  # noqa: CCR001
        """
        Get a value for a given type.

        :param field_type: Field type, as declared in models
        :param value: Value to de-serialize
        :return: De-serialized value
        """
        field_origin = get_origin(field_type)

        if field_origin == list:
            field_args = get_args(field_type)
            return [cls.get_value_for_type(field_args[0], _val) for _val in value]

        if field_origin == Union:
            field_args = get_args(field_type)
            # Optional
            if len(field_args) == 2 and type(None) in field_args:
                _field_args = [field_arg for field_arg in field_args if not isinstance(field_arg, type(None))]
                return cls.get_value_for_type(_field_args[0], value)
            # Union
            else:
                for field_arg in field_args:
                    try:
                        return cls.get_value_for_type(field_arg, value)
                    except TypeError:
                        pass

        if issubclass(field_type, enum.Enum):
            return field_type(value)

        if issubclass(field_type, Model):
            return field_type.from_json_data(value)

        return value

    @classmethod
    def from_json_data(cls, json_data: Dict[str, Any]) -> "Model":
        """
        Convert de-serialized data to a output_model instance.

        :param json_data:
        :return:
        """
        parameters = {}
        for key, val in json_data.items():
            cleaned_key = to_snake_case(key)
            cleaned_value = cls.get_cleaned_value(cleaned_key, val)
            parameters.update({cleaned_key: cleaned_value})
        return cls(**parameters)

    def __eq__(self, other):
        """
        Test if current instance is equal to other one.

        :param other: Other output_model instance
        :return: `True` if self == other
        """
        if self.__class__ != other.__class__:
            return False

        filtered_self = fields(self)
        for field in filtered_self:
            val_self = getattr(self, field)
            val_other = getattr(other, field)
            if val_self != val_other:
                return False
        return True


@dataclass()
class Metadata(Model):
    """Generic metadata."""

    name: str
    value: str

    def clean(self) -> None:
        """Check some fields."""
        if self.name and len(self.name) > 64:
            raise ModelValidationError("name", f"Too long: {len(self.name)} > 64")
        if self.value and len(self.value) > 1024:
            raise ModelValidationError("value", f"Too long: {len(self.value)} > 1024")


@dataclass()
class DocumentData(Model):
    """Document data."""

    name: str
    description: Optional[str] = None
    reference: Optional[str] = None
    confidentiality: ConfidentialityLevel = ConfidentialityLevel.Normal
    priority: PriorityLevel = PriorityLevel.Normal
    sequential_identifier: Optional[str] = None
    comment: Optional[str] = None
    external_recipients: Optional[List[str]] = None
    metadatas: Optional[List[Metadata]] = None

    def clean(self):
        """Check some fields."""
        if len(self.comment) > 1024:
            raise ModelValidationError("name", f"Too long: {len(self.name)} > 255")
        if self.description and len(self.description) > 1024:
            raise ModelValidationError("description", f"Too long: {len(self.description)} > 1024")
        if self.comment and len(self.comment) > 1024:
            raise ModelValidationError("comment", f"Too long: {len(self.comment)} > 1024")
        if self.reference and len(self.reference) > 64:
            raise ModelValidationError("reference", f"Too long: {len(self.reference)} > 64")


@dataclass()
class FileInput(Model):
    """File input data."""

    id: str
    name: str
    description: Optional[str] = None


@dataclass()
class ProcedureInput(Model):
    """File input data."""

    rack_code: str
    document_data: DocumentData
    files: List[FileInput]


@dataclass()
class ProcedureFile(Model):
    """ProcedureFile."""

    __api_entry__ = "api/ProcedureFiles"

    id: str
    name: str
    size: int
    mime_type: Optional[str] = None
    description: Optional[str] = None
    version: Optional[str] = None
    is_digitally_signed: bool = False


@dataclass()
class WorkflowEvent(Model):
    """WorkflowEvent."""

    id: int
    task_type: WorkflowTaskType
    task_status: WorkflowTaskStatus
    event_date: Optional[datetime] = None
    event_actor: Optional[str] = None
    comment: Optional[str] = None


@dataclass()
class ProcedureOutput(Model):
    """Procedure output."""

    id: str
    document_data: DocumentData
    workflow_history: List[WorkflowEvent]
    files: List[ProcedureFile]
    status: ProcedureStatus = ProcedureStatus.Initialized


@dataclass()
class Procedure(Model):
    """Procedure."""

    __api_entry__ = "api/Procedures"

    rackCode: str
    document_data: DocumentData

"""
enums for :mod:`elise_client.models` application.

:creationdate: 07/06/2021 15:08
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client.models.enums

"""
import enum
import logging

__author__ = "fguerin"
logger = logging.getLogger("elise_client.models.enums")


class ProcedureFileFormat(enum.Enum):
    """File format."""

    Original = "Original"
    Pdf = "Pdf"
    Signed = "Signed"
    Signature = "Signature"


class ConfidentialityLevel(enum.Enum):
    """Confidentiality level."""

    Normal = "Normal"
    Confidential = "Confidential"
    Personal = "Personal"


class PriorityLevel(enum.Enum):
    """Priority level."""

    Normal = "Normal"
    Important = "Important"
    High = "High"
    Low = "Low"


class ProcedureStatus(enum.Enum):
    """Procedure status."""

    Initialized = "Initialized"
    Processing = "Processing"
    Validating = "Validating"
    Validated = "Validated"
    Signing = "Signing"
    Signed = "Signed"
    NonValidated = "NonValidated"
    Canceled = "Canceled"
    Error = "Error"


class WorkflowTaskType(enum.Enum):
    """Workflow Task Type."""

    Processing = "Processing"
    Validation = "Validation"
    Signature = "Signature"
    Information = "Information"


class WorkflowTaskStatus(enum.Enum):
    """Workflow Task Status."""

    Inactive = "Inactive"
    Pending = "Pending"
    Ongoing = "Ongoing"
    Completed = "Completed"
    IsNotValidated = "IsNotValidated"
    Canceled = "Canceled"

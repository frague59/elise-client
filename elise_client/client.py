"""
Client for :mod:`elise_client` application.

:creationdate: 25/05/2021 16:18
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client.client

"""
import logging
import mimetypes
import pprint
from copy import deepcopy
from json import JSONDecodeError
from pathlib import Path
from typing import Any, Dict, List, NamedTuple, Optional, Tuple, Type, Union

import requests

from . import helpers, models

__author__ = "fguerin"
logger = logging.getLogger("elise_client.client")

DEFAULT_HEADERS: Dict[str, Any] = {
    "Accept": "application/json",
}

DEFAULT_QUERY_PARAMS: Dict[str, Any] = {
    "api-version": "1.0",
}

DEFAULT_LANGUAGE_CODE = "FR-fr"


class ApiResponse(NamedTuple):
    """API response code and data."""

    status_code: int
    data: Optional[
        Union[
            models.Model,
            List[models.Model],
            int,
            bytes,
        ]
    ]


class EliseClient:
    """Client for the ELISE API."""

    _auth_key: Optional[str] = None
    _session: Optional[requests.Session] = None

    # region Magic methods

    def __init__(self, base_url: str, instance_name: str, application_key: str, username: str, password: str):
        """
        Initialize the client.

        .. warning:: The client MUST be initialized in a `with` statement.


        :param base_url: Base server URL
        :param instance_name: Application instance name
        :param application_key: Application field
        :param username: User name
        :param password: User password
        """
        self._base_url: str = base_url
        self._instance_name: str = instance_name
        self._application_key: str = application_key
        self._username: str = username
        self._password: str = password

    def __enter__(self) -> "EliseClient":
        """
        Authenticate the client and set a Session.

        .. note:: Magic method to use as context manager, aka. `with` statement

        :return: Self instance
        """
        if self._session is not None:
            raise RuntimeError("Multiple with statements on API call.")

        _headers = deepcopy(DEFAULT_HEADERS)
        # Grab the authorization key
        self._auth_key = self._get_auth_key()
        _headers.update({"Authorization": self._auth_key})
        # Create the requests session
        self._session = requests.Session()
        self._session.headers.update(_headers)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        """
        Drop the session and delete.

        :param exc_type: Manager Exec type
        :param exc_val: Manager Exec value
        :param exc_tb: Manager Exec traceback
        :return: Nothing
        """
        if self._session is not None:
            self._session.close()
            self._session = None
            self._auth_key = None

    # endregion Magic methods

    # region Connection / Authentication
    @property
    def is_connected(self):
        """
        Check in the client is connected AND authenticated.

        :return: if `True`, the client is connected and authorized
        """
        return self._session and self._auth_key

    @property
    def session(self) -> requests.Session:
        """
        Get the requests session object.

        :return: Connected and authorized session
        """
        if self._session is None or self._auth_key is None:
            raise helpers.ClientError(f"Client not connected to {self._base_url} server.")

        return self._session

    def _get_auth_key(self) -> str:
        json_data = {
            "applicationKey": self._application_key,
            "instance": self._instance_name,
            "language": DEFAULT_LANGUAGE_CODE,
            "username": self._username,
            "password": self._password,
        }
        resp = requests.post(
            url=helpers.format_url(self._base_url, "api/Tokens", query_params={"api-version": "1.0"}),
            headers=DEFAULT_HEADERS,
            json=json_data,
        )
        if resp.status_code not in (200, 201):
            raise helpers.ClientError(
                f"Unable to authenticate to server {self._base_url} with user "
                f"{self._username}: {resp.status_code}: {resp.reason}"
            )
        _auth_key = resp.content.decode("utf-8").strip('"')
        logger.debug(f"EliseClient::_get_auth_key() _auth_key = {_auth_key}")
        return _auth_key

    # endregion Connection / Authentication

    # region API interactions
    def api_call(
        self,
        method: str,
        url: str,
        output_model: Optional[Type[models.Model]] = None,
        url_formatting: Optional[Dict[str, str]] = None,
        query_params: Optional[Dict[str, str]] = None,
        multiple: bool = False,
        json_data: Optional[Dict[str, Any]] = None,
        headers: Optional[Dict[str, str]] = None,
        files: Optional[Dict[str, Tuple[Any, ...]]] = None,
    ) -> ApiResponse:
        """
        Call the API endpoint.

        :param method: HTTP Method used for query, line `GET`, `PUT`, `POST`
        :param url: API endpoint URL
        :param output_model: Model class to use for command output deserialization
        :param url_formatting: Mapping on formatting for URL
        :param query_params: Additional query params
        :param multiple: If `True`, a list of objects will be returned, otherwise a single one
        :param json_data: JSON-serializable data, passed to the query
        :param headers: Additional headers, merged with original session headers
        :param files: Files to upload to the server
        :return: Constructed object or list of objects, or raw response content
        """
        _query_params = query_params or {}
        _query_params.update(DEFAULT_QUERY_PARAMS)

        _final_url = helpers.format_url(
            base_url=self._base_url,
            url=url,
            url_formatting=url_formatting,
            query_params=_query_params,
        )

        assert method in ("GET", "POST", "DELETE", "PUT")

        # Prepare the query
        _request_params: Dict[str, Any] = {
            "method": method,
            "url": _final_url,
        }

        if json_data:
            _request_params["json"] = json_data

        if headers:
            _request_params["headers"] = headers

        if files:
            _request_params["files"] = files

        logger.debug(f"EliseClient::api_call() request_params = {pprint.pformat(_request_params)}")

        # Fetch query !
        resp = self.session.request(**_request_params)
        if resp.status_code not in range(200, 300):
            raise helpers.ClientError(
                f"Error api call: {resp.status_code}: {resp.reason}\nmethod:{method} "
                f"/ url: {_final_url}\njson_data: {pprint.pformat(json_data)}"
                f"\nfiles: {files}"
                f"\ncontent: {resp.content!r}"
            )

        return self._format_response(
            resp=resp,
            final_url=_final_url,
            multiple=multiple,
            output_model=output_model,
        )

    @staticmethod
    def _format_response(
        resp: requests.Response,
        final_url: str,
        multiple: bool = False,
        output_model: Optional[Type[models.Model]] = None,
    ) -> ApiResponse:
        try:
            response_data = resp.json()
            logger.debug(f"EliseClient::_format_response() response: {pprint.pformat(response_data)}")
        except JSONDecodeError:
            logger.warning(
                f"EliseClient::_format_response() Unable to decode JSON from data for url: {final_url}"
                f"\n{resp.content!r}"
            )
            return ApiResponse(status_code=resp.status_code, data=resp.content)

        if output_model:
            if multiple:
                return ApiResponse(
                    status_code=resp.status_code,
                    data=[output_model.from_json_data(_data) for _data in response_data],
                )
            return ApiResponse(
                status_code=resp.status_code,
                data=output_model.from_json_data(response_data),
            )
        return ApiResponse(
            status_code=resp.status_code,
            data=response_data,
        )

    # endregion API interactions

    # region Public methods
    def send_file(self, file_path: Path, description: Optional[str] = None) -> models.FileInput:
        """
        Send a file to server, get get the file id.

        :param file_path: Path to the file
        :param description: Additional optional description
        :return: :class:`elise_client.models.FileInput` instance
        """
        ct, _ = mimetypes.guess_type(file_path)
        with file_path.open("rb") as content_file:
            api_response = self.api_call(
                method="POST",
                url="api/Files",
                files={"file": (file_path.name, content_file, ct)},
            )

            return models.FileInput(
                id=str(api_response.data),
                name=str(file_path.name),
                description=description,
            )

    def create_procedure(
        self,
        rack_code: str,
        name: str,
        input_files: List[models.FileInput],
        description: Optional[str] = None,
        comment: Optional[str] = None,
        external_recipients: Optional[List[str]] = None,
        metadatas: Optional[Dict[str, Any]] = None,
    ) -> ApiResponse:
        """
        Create a procedure for a given file.

        :param rack_code: Rack code
        :param name: Procedure name
        :param input_files: List of files to add to create the procedure for
        :param description: Procedure description
        :param comment: Procedure comment
        :param external_recipients: Additional recipients
        :param metadatas: Additional metas
        :return: Procedure identifier
        """
        document_data_params: Dict[str, Any] = {
            "name": name,
            "description": description,
            "comment": comment,
        }
        if external_recipients is not None:
            document_data_params.update({"external_recipients": external_recipients})

        if metadatas:
            document_data_params.update(
                {
                    "metadatas": [models.Metadata(name=name, value=value) for name, value in metadatas.items()],
                }
            )

        procedure = models.ProcedureInput(
            rack_code=rack_code,
            document_data=models.DocumentData(**document_data_params),
            files=input_files,
        )

        _final_url = helpers.format_url(
            base_url=self._base_url,
            url="api/Procedures",
        )

        json_data = procedure.to_json_data()
        resp = self.api_call(
            method="POST",
            url=_final_url,
            json_data=json_data,
        )
        return resp

    def get_procedure(
        self,
        proc_id: str,
    ) -> ApiResponse:
        """
        Get procedure data.

        :param proc_id: Procedure ID
        :return: Procedure data object
        """
        api_response = self.api_call(
            method="GET",
            url="/api/Procedures/{id}",
            url_formatting={"id": proc_id},
            output_model=models.ProcedureOutput,
        )
        return api_response

    def delete_procedure(self, proc_id: str) -> ApiResponse:
        """
        Delete procedure files.

        :param proc_id: Procedure ID
        """
        result = self.api_call(
            method="DELETE",
            url="/api/Procedures",
            query_params={"id": proc_id},
        )
        return result

    def get_procedure_file(
        self,
        proc_id: str,
        file_id: str,
        file_format: models.ProcedureFileFormat = models.ProcedureFileFormat.Original,
    ) -> models.ProcedureFile:
        """
        Get a procedure file from server.

        :param proc_id: Procedure identifier
        :param file_id: File identifier
        :param file_format: Procedure file format
        :return: Procedure file
        """
        procedure_file = self.api_call(
            method="GET",
            url="/api/ProcedureFiles",
            query_params={
                "procedureId": proc_id,
                "fileId": file_id,
                "format": file_format.value,
            },
        )
        logger.debug(f"EliseClient::get_procedure_file() procedure_file = {procedure_file!r}")
        return procedure_file  # type: ignore

    # endregion Public methods

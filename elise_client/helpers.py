"""
helpers for :mod:`elise_client` application.

:creationdate: 08/06/2021 12:17
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: elise_client.helpers

"""
import logging
from typing import Any, Dict, Optional
from urllib.parse import urljoin

from requests import PreparedRequest

__author__ = "fguerin"
logger = logging.getLogger("elise_client.helpers")


def format_url(
    base_url: str,
    url: str,
    url_formatting: Optional[Dict[str, Any]] = None,
    query_params: Optional[Dict[str, Any]] = None,
) -> str:
    """
    Format the final URL.

    :param base_url:
    :param url:
    :param url_formatting:
    :param query_params:
    :return: Formatted URL
    """
    _url = urljoin(base_url, url)
    if url_formatting:
        _url = _url.format(**url_formatting)
    if query_params:
        req = PreparedRequest()
        req.prepare_url(_url, query_params)
        _url = req.url  # type: ignore
    logger.debug(f"EliseClient::format_url() _url = {_url}")
    return _url


class ClientError(Exception):
    """Generic error raised by the :class:`elise_client.client.EliseClient`."""

    pass
